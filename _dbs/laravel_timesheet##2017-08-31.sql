# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.5.5-10.2.8-MariaDB)
# Database: laravel_timesheet
# Generation Time: 2017-09-01 00:43:37 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`migration`, `batch`)
VALUES
	('2015_09_24_202751_users_table',1),
	('2015_09_24_202754_posts_table',1),
	('2015_09_24_202823_roles_table',1),
	('2015_09_24_202842_comments_table',1),
	('2015_09_24_202904_password_resets_table',1),
	('2015_09_27_001958_timecards_table',1),
	('2015_09_27_002011_projects_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `post_content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `image_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment_status` tinyint(1) NOT NULL,
  `comment_count` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_image_path_unique` (`image_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table projects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;

INSERT INTO `projects` (`id`, `created_at`, `updated_at`, `name`, `description`, `status`, `user_id`, `deleted_at`)
VALUES
	(1,'0000-00-00 00:00:00','0000-00-00 00:00:00','reliance','Reliance',1,1,NULL),
	(2,'0000-00-00 00:00:00','0000-00-00 00:00:00','reliance','Reliance',1,1,NULL);

/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `description`)
VALUES
	(1,'admin','All operations permitted. Role, User, Project Management '),
	(2,'payroll-admin','Operations such as User & Timecard management'),
	(3,'project-manager','Operations such as manage project, approve/edit/delete timecard'),
	(4,'employee','System User; Timecard Entry');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table timecards
# ------------------------------------------------------------

DROP TABLE IF EXISTS `timecards`;

CREATE TABLE `timecards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `username` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_id` bigint(20) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `created_at`, `updated_at`, `username`, `password`, `first_name`, `last_name`, `active`, `email`, `remember_token`, `role_id`, `deleted_at`)
VALUES
	(1,'2017-08-10 17:16:21','0000-00-00 00:00:00','media','$2y$10$k0Q7bYYFZiAdusCGpYwzMuV42kNYkw7zpEI/6tjzo1vQz0xxvz3e2','media','master',1,'media@gmail.com',NULL,1,NULL),
	(2,'2017-08-10 17:16:21','0000-00-00 00:00:00','norman','$2y$10$XNDV/.JGIxP3bINzIk038uXU7zZo1PndXFuuYpMQdUPJwqekgSX3G','norman','cote',1,'norman@gmail.com',NULL,2,NULL),
	(3,'2017-08-10 17:16:21','0000-00-00 00:00:00','chris','$2y$10$q/QqvsrhstDzyVsT5t8ePegnodINbdqXClFgaqweoE9mPRXoY61bS','chris','roger',1,'chris@gmail.com',NULL,3,NULL),
	(4,'2017-08-10 17:16:21','0000-00-00 00:00:00','F2kinN5JcL','$2y$10$NSyX1SX7LRhzr4ddrjGmiun6KOjvlDVbG2A37F01D0TEMnvF6cr1.','t9Vj5yhYHw','4yusTYNMiI',1,'4xqCjv4hEy@gmail.com',NULL,1,NULL),
	(5,'2017-08-10 17:16:21','0000-00-00 00:00:00','DNa2r6DfhA','$2y$10$aHNXd0Vn/Otjlto/x4c.Yu/K1LHgDyuQsiBFzwt5GXnhII8p90ib6','ACcOmjtLIj','hQztJ5rmav',1,'f3RKEoWIEy@gmail.com',NULL,1,NULL),
	(6,'2017-08-10 17:16:21','0000-00-00 00:00:00','TBBZaXQmz2','$2y$10$0ORvLPpJJR7B4AFl2suGiuiwqC.oYl60LJsUylyg07XpTEe4Y09S.','xHbCl66Kgs','A5BiWQChRr',1,'wqVj7Ep9Zh@gmail.com',NULL,1,NULL),
	(7,'2017-08-10 17:16:22','0000-00-00 00:00:00','0oyFW9wUmQ','$2y$10$.vQWZwuSgsbERO8EiVAUzekwaDTX93/oawVpRR0bLlf4YBMj.Rb9S','FNUmPJ0X6j','nfI5Oz2AXC',1,'0RgCdRNt92@gmail.com',NULL,1,NULL),
	(8,'2017-08-10 17:16:22','0000-00-00 00:00:00','J2HxbNYBIX','$2y$10$qa2MExHsz7KEFfI5fNZImO3nshKMxSQxnuBW7lzuVrpInSXX0nWeK','DxVVF6e6Fv','2je6x5QYxC',1,'kEr1ZSYhEM@gmail.com',NULL,1,NULL),
	(9,'2017-08-10 17:16:22','0000-00-00 00:00:00','9mtOYxtedM','$2y$10$8SKaUAQf3N9JcwtD2wG7mONtDjCg9FeHTqvyFkryka9IHajULKxkq','EaBZAMiGqW','mJTPsQYL2z',1,'tAJUt3Qhtp@gmail.com',NULL,1,NULL),
	(10,'2017-08-10 17:16:22','0000-00-00 00:00:00','ljaGWoXAQC','$2y$10$zkfBMmM1LvhTrxoQrC0DOe7WizP47axt6mjTPssnabKL7G5XyH4RS','zLDnWcrZvG','02Zr65UEaO',1,'5CmHpk4lGT@gmail.com',NULL,1,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
