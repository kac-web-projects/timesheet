<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;
use League\Flysystem\Filesystem;
use Log;

class ClearAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear-all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Compiled, Expired, Application, Route, Configuration caches & tokens';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info("Command : ClearAll");

        $this->info("Clearing All..");
 
        // Clear all compiled view files
        $this->call('view:clear');
        
        // Flush expired password reset tokens
        $this->call('auth:clear-resets');

        // Flush the application cache
        $this->call('cache:clear');

        // Remove the route cache file
        $this->call('route:clear');

        // Remove the configuration cache file
        $this->call('config:clear');

        $this->info("Done");
    }
}