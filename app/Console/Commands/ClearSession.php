<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;
use League\Flysystem\Filesystem;
use Log;

class ClearSession extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear-session';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'We prefer Employee(user) to enter his password every day for security reason';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info("Command : ClearSession");

        $this->info("Clearing Session..");
 
        $storageFramework = 'storage/framework/';
        Storage::deleteDirectory($storageFramework . 'sessions');
        Storage::makeDirectory($storageFramework . 'sessions');
 
        $this->info("Done");
    }
}