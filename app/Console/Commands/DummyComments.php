<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

// Additional
use Storage;
use League\Flysystem\Filesystem;
use Log;

class DummyComments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dummy-comments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add dummy Comments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info("Command : DummyComments");

        $this->info("Adding Dummy Comments..");

        factory('App\Models\Comment', 5)->create();

        $this->info("Done");
    }
}