<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;
use League\Flysystem\Filesystem;

class DummyData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dummy-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Users, Posts, Comments, Projects & Timecards';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Dummy All..");
 
        // Add dummy Users
        $this->call('dummy-users');
        
        // Add dummy Posts
        // $this->call('dummy-posts');

        // Add dummy Comments
        $this->call('dummy-comments');

        // Add dummy Projects
        $this->call('dummy-projects');

        // Add dummy Timecards
        $this->call('dummy-timecards');

        $this->info("Done");
    }
}