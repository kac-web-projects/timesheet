<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

// Additional
use Storage;
use League\Flysystem\Filesystem;
use Log;

class DummyPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dummy-posts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add dummy Posts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info("Command : DummyPosts");

        $this->info("Adding Dummy Posts..");

        factory('App\Models\Post', 5)->create();

        $this->info("Done");
    }
}