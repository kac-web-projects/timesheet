<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

// Additional
use Storage;
use League\Flysystem\Filesystem;
use Log;

class DummyProjects extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dummy-projects';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add dummy Projects';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info("Command : DummyProjects");

        $this->info("Adding dummy Projects..");

        factory('App\Models\Project', 5)->create();

        $this->info("Done");
    }
}