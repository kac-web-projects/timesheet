<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

// Additional
use Storage;
use League\Flysystem\Filesystem;
use Log;

class DummyTimecards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dummy-timecards';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add dummy Timecards';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info("Command : DummyTimecards");

        $this->info("Adding dummy Timecards..");

        factory('App\Models\Timecard', 5)->create();

        $this->info("Done");
    }
}