<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

// Additional
use Storage;
use League\Flysystem\Filesystem;
use Log;

class DummyUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dummy-users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add dummy Users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info("Command : DummyUsers");

        $this->info("Adding dummy Users..");

        factory('App\Models\User', 5)->create();

        $this->info("Done");
    }
}