<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Storage;
use League\Flysystem\Filesystem;

class SystemCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'system-check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check roles table, run unit test, check all Urls are protected & more';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("SystemCheck Started..");
 
        //TODO
        
        $this->info("Done");
    }
}