<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\DummyData::class,
        \App\Console\Commands\DummyUsers::class,
        \App\Console\Commands\DummyPosts::class,
        \App\Console\Commands\DummyComments::class,
        \App\Console\Commands\DummyProjects::class,
        \App\Console\Commands\DummyTimecards::class,
        \App\Console\Commands\ClearSession::class,
        \App\Console\Commands\ClearAll::class,
        \App\Console\Commands\SystemCheck::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('clear-all')
                 ->hourly();

        $schedule->command('clear_session')->dailyAt('3:00');

        // $schedule->command('clear_all')->everyFiveMinutes();
    }
}