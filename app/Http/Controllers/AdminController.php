<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

// Additional
use Log, Input, Validator;
use Session, Redirect;
use Auth;
use App\Models\User;
use Artisan;

class AdminController extends Controller
{
    
    /**
     * Display Dashboard
     *
     * @return Response
     */
    public function showDashboard()
    {
        Log::info("AdminController::showDashboard()");

        $user = User::findOrFail(Auth::id())->first();

        return View('admin.dashboard')->with('user', $user);
    }


    /**
     * Debug GUI
     *
     * @return Response
     */
    public function showDebug()
    {
        Log::info("AdminController::showDebug()");

        $user = User::findOrFail(Auth::id())->first();

        return View('admin.debug')->with('user', $user);
    }    

    /**
     * Debug GUI
     *
     * @return Response
     */
    public function runCmd($type)
    {
        Log::info("AdminController::runCmd($type)");

        switch ($type) {
            case 'system-check':
                $exitCode = Artisan::call('system_check');
                Session::flash('caption', 'Operation');
                Session::flash('content', 'Views Cleared');
                return 'yup';
                break;
            
            case 'view-clear':
                $exitCode = Artisan::call('view:clear');
                Session::flash('caption', 'Operation');
                Session::flash('content', 'Views Cleared');
                break;
            
            case 'clear-session':
                $exitCode = Artisan::call('clear_session');
                break;
            
            case 'migrate-refresh':
                $exitCode = Artisan::call('migrate:refresh');
                break;

            case 'migrate-reset':
                $exitCode = Artisan::call('migrate:reset');
                break;

            case 'migrate-rollback':
                $exitCode = Artisan::call('migrate:rollback');
                break;

            case 'migrate-status':
                $exitCode = Artisan::call('migrate:status');
                break;

            default:
                # code...
                break;
        }


        return $exitCode;
        // migrate:install     Create the migration repository
        // migrate:refresh     Reset and re-run all migrations
        // migrate:reset       Rollback all database migrations
        // migrate:rollback    Rollback the last database migration
        // migrate:status

        if($exitCode == 0) {
            Session::flash('type', 'Notify');
        }
        else {
            Session::flash('type', 'Warning');
            Session::flash('caption', 'Try Again');
            Session::flash('content', 'Something went Wrong');
        }
        // $exitCode = Artisan::call('rebuild_views');

        // notify
        Session::flash('caption', 'Operation');

        return Redirect::to('debug');

        // return $exitCode;
        // return $type;
        // Log::info("AdminController::showDebug()");

        // $user = User::findOrFail(Auth::id())->first();

        // return View('admin.debug')->with('user', $user);
    }    
}