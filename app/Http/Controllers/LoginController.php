<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

// Additional
use App\Models\User;
use Auth;
use Input;
use Log;
use Redirect;
use Session;
use Validator;
use Illuminate\Http\Request;

class LoginController extends Controller
{

    // define validation rules
    public static $rules = array(
        'username' => 'required',
        'password' => 'required',
    );

    /**
     * Basic Middleware (Filter)
     *
     */
    public function __construct()
    {
        $this->middleware('auth', array('only' => array('showDashboard')));
    }

    /**
     * Display Login Form
     *
     * @return Response
     */
    public function showLogin()
    {
        Log::info("LoginController::showLogin()");

        //Usage : If Logged-In, redirect to Home else to Login
        if (Auth::check()) {

            // notify
            Session::flash('type', 'Warning');
            Session::flash('caption', 'Login');
            Session::flash('content', 'You are already Logged-In');

            Log::info("User already Logged");

            // redirect
            return Redirect::to('/');
        } else {
            Log::info("User not Logged");

            return View('authentication.login');
        }
    }

    /**
     * Attempt to Login and redirected if success
     *
     * @return Response
     */
    public function doLogin()
    {
        Log::info("LoginController::doLogin()");

        $in = Input::all();

        $validator = Validator::make($in, self::$rules);

        // validate
        if ($validator->fails()) {

            Log::info("Validation : Failed while Login-Attempt");

            // track the error messages from the validator
            Log::debug($validator->messages());

            // notify
            Session::flash('type', 'Warning');
            Session::flash('caption', 'Login');
            Session::flash('content', 'Validation Failed');

            // redirect
            return Redirect::to('login')
                ->withErrors($validator)
                ->withInput($in);
        } else {

            $rememberMe = isset($in['remember_me']) ? 1 : 0;

            $viaUser  = Auth::attempt(['username' => $in['username'], 'password' => $in['password']], $rememberMe);
            $viaEmail = Auth::attempt(['email' => $in['username'], 'password' => $in['password']], $rememberMe);

            if ($viaUser || $viaEmail) {

                // notify
                Session::flash('type', 'Notify');
                Session::flash('caption', 'Login');
                Session::flash('content', 'Successfully');

                Log::debug(Auth::user());

                //NOTE : Destination URL feature
                $destination = $in['destination'];

                if (isset($destination)) {
                    return Redirect::to($destination);
                } else {
                    // redirect
                    return Redirect::to('/');
                }

            } else {

                // notify
                Session::flash('type', 'Warning');
                Session::flash('caption', 'Login');
                Session::flash('content', 'Invalid credentials');

                // redirect
                return Redirect::to('login');
            }

        }
    }

    /**
     * LogOut current User if Logged-In
     *
     * @return Response
     */
    public function doLogOut()
    {
        Log::info("LoginController::doLogOut()");

        if (Auth::check()) {

            Auth::logout();

            // notify
            Session::flash('type', 'Notify');
            Session::flash('caption', 'Logout');
            Session::flash('content', 'You are Logout successfully');

            return View('authentication.logout');
        } else {

            // notify
            Session::flash('type', 'Warning');
            Session::flash('caption', 'Logout');
            Session::flash('content', 'You are already Logged-Out');

            return View('authentication.logout');
        }
    }

}
