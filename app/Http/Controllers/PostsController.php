<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

// Additional
use App\Models\Post, App\Models\User;
use Log, Input, Validator;
use Session, Redirect;
use Auth;

class PostsController extends Controller
{
    // define validation rules
    public static $rules = array(
            'post_title'       => 'required',
            'post_content'     => 'required',
            'post_status'      => 'required',
            'image' => 'mimes:jpg,jpeg,png,gif|max:500'
        );

    // Media upload
    public static $uploadPath = 'uploads/';


    /**
     * Show News Feed
     *
     * @return \Illuminate\Http\Response
     */
    public function newsFeed()
    {
        Log::info("PostsController::newsFeed()");

        $posts = Post::all();

        return View('posts.news_feed')->with('posts', $posts);
    }


    /**
     * Listing User's own Posts
     *
     * @return \Illuminate\Http\Response
     */
    public function userPosts()
    {
        Log::info("PostsController::userPosts()");

        $posts = Post::where('user_id', Auth::id())->get();

        return View('posts.user_posts')->with('posts', $posts);
    }


    /**
     * Listing Posts
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info("PostsController::index()");

        $posts = Post::all();

        return View('posts.index')->with('posts', $posts);
    }


    /**
     * Show the form for creating a new Post
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Log::info("PostsController::create()");

        return View('posts.create');
    }

    /**
     * Store a newly created Post in DB
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info("PostsController::store()");

        $in = Input::all();

        $validator = Validator::make($in, self::$rules);

        // validate
        if ($validator->fails()) {
        
            Log::debug("Validation : Failed while Storing Post");

            // track the error messages from the validator
            Log::debug($validator->messages());

            // notify
            Session::flash('type', 'Warning');
            Session::flash('caption', 'Post');
            Session::flash('content', 'Validation Failed');
            
            // redirect
            return Redirect::to('post/create')
                ->withErrors($validator)
                ->withInput($in);
        } else {

            // store
            $post                     = new Post();
            $post->post_title         = $in['post_title'];
            $post->post_content       = $in['post_content'];
            $post->post_status        = $in['post_status'];
            $post->user_id            = Auth::id();
            $post->comment_status     = isset($in['comment_status']) ? 1 : 0;
        
            if (Input::hasFile('image')) {
    
                $filename = $in['image']->getClientOriginalName();
                Input::file('image')->move(self::$uploadPath, $filename);
                $post->image_path     = self::$uploadPath . $filename;
            }
    
            $post->save();

            // notify
            Session::flash('type', 'Notify');
            Session::flash('caption', 'Post');
            Session::flash('content', 'Created Successfully');

            // redirect
            return Redirect::to('post');
        }
    }

    /**
     * Display the specified Post
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Log::info("PostsController::show($id)");

        //TODO : Save Hits for this page

        // retrieve particular post
        $post = Post::findOrFail($id);

        // retrieve current user if Logged-In
        $currentUser = null;
        if(Auth::check()) {
            $currentUser = User::findOrFail(Auth::id())->first();
        }

        // show the view and pass the post to it
        return View('posts.show')
            ->with('post', $post)
            ->with('currentUser', $currentUser);
    }

    /**
     * Show the form for editing the specified Post
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Log::info("PostsController::edit($id)");

        // retrieve particular post
        $post = Post::findOrFail($id);

        // retrieve current user if Logged-In
        $currentUser = null;
        if(Auth::check()) {
            $currentUser = User::findOrFail(Auth::id())->first();
        }

        return View('posts.edit')
            ->with('post', $post)
            ->with('currentUser', $currentUser);
    }

    /**
     * Update the specified Post in DB
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
       Log::info("PostsController::update($id)");

        $in = Input::all();
        $validator = Validator::make($in, self::$rules);

        // validate
        if ($validator->fails()) {
        
            Log::debug("Validation : Failed while Updating Post-Id : $id");

            // track the error messages from the validator
            Log::debug($validator->messages());

            // notify
            Session::flash('type', 'Warning');
            Session::flash('caption', 'Post');
            Session::flash('content', 'Validation Failed');

            // redirect
            return Redirect::to("post/$id/edit")
                ->withErrors($validator)
                ->withInput($in);
        } else {

            // update
            $post                 = Post::findOrFail($id);
            $post->post_title     = $in['post_title'];
            $post->post_content   = $in['post_content'];
            $post->post_status    = $in['post_status'];
            $post->user_id        = Auth::id();
            $post->comment_status = isset($in['comment_status']) ? 1 : 0;

            if (Input::hasFile('image')) {
    
                $filename = $in['image']->getClientOriginalName();
                Input::file('image')->move(self::$uploadPath, $filename);
                $post->image_path     = self::$uploadPath . $filename;
            }
    
            $post->save();

            // notify
            Session::flash('type', 'Notify');
            Session::flash('caption', 'Post');
            Session::flash('content', 'Updated Successfully');
            
            // redirect
            return Redirect::to("post/$id/edit");
        }

    }

    /**
     * Remove the specified Post from DB
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}