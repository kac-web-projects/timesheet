<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

// Additional
use App\Models\User;
use App\Models\Roles;
use App\Models\Project;
use Log, Input, Validator;
use Session, Redirect, Hash;

class ProjectController extends Controller
{
    // define validation rules
    public static $rules = array(
            'name'  => 'required|unique:project',
            'description'     => 'required',
            'status'     => 'required',
        );


    /**
     * Listing Projects
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info("ProjectController::index()");

        $projects = Project::all();

        return View('projects.index')->with('projects', $projects);
    }

    /**
     * Show the form for creating a new Project
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Log::info("ProjectController::create()");

        $users = User::lists('username', 'id');

        return View('projects.create')->with('users', $users);
    }

    /**
     * Store a newly created Project in DB
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       Log::info("ProjectController::store()");

        $in = Input::all();

        $validator = Validator::make($in, self::$rules);

        // validate
        if ($validator->fails()) {
        
            Log::info("Validation : Failed while Storing Project");

            // track the error messages from the validator
            Log::debug($validator->messages());

            // notify
            Session::flash('type', 'Warning');
            Session::flash('caption', 'Project');
            Session::flash('content', 'Validation Failed');

            // redirect
            return Redirect::to('project/create')
                ->withErrors($validator)
                ->withInput($in);
        } else {

            // store
            $project = new Project();
            $project->name        = $in['name'];
            $project->description = $in['description'];
            $project->user_id     = 1; //TODO : Via Session
            $project->save();

            // notify
            Session::flash('type', 'Notify');
            Session::flash('caption', 'Timecard');
            Session::flash('content', 'Created Successfully');
    
            // redirect
            return Redirect::to("project");
            // return Redirect::to("project/$project->id");
        }
    }

    /**
     * Display the specified Project
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Log::info("ProjectController::show($id)");

        // retrieve particular Project
        $project = Project::findOrFail($id);

        // show the view and pass the post to it
        return View('projects.show');
    }

    /**
     * Show the form for editing the specified Project
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Log::info("ProjectController::edit($id)");

        // retrieve particular Project
        $project = Project::findOrFail($id);

        // show the view and pass the post to it
        return View('projects.edit');
    }

    /**
     * Update the specified Project in DB
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
       Log::info("ProjectController::update($id)");

        $in = Input::all();

        $validator = Validator::make($in, self::$rules);

        // validate
        if ($validator->fails()) {
        
            Log::debug("Validation : Failed while Updating Project-Id : $id");

            // track the error messages from the validator
            Log::debug($validator->messages());

            // notify
            Session::flash('type', 'Warning');
            Session::flash('caption', 'Project');
            Session::flash('content', 'Validation Failed');
            
            // redirect
            return Redirect::to("project/$id/edit")
                ->withErrors($validator)
                ->withInput($in);
        } else {

            // update
            $project = Project::findOrFail($id);
            $project->name        = $in['name'];
            $project->description = $in['description'];
            $project->user_id     = 1; //TODO : Via Session
            $project->save();

            // notify
            Session::flash('type', 'Notify');
            Session::flash('caption', 'Project');
            Session::flash('content', 'Updated Successfully');

            // redirect
            return Redirect::to("project/$id/edit");
        }

    }

    /**
     * Remove the specified Project from DB
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}