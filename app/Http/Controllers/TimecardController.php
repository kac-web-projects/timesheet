<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

// Additional
use App\Models\User;
use App\Models\Roles;
use App\Models\Timecard;
use App\Models\Project;

use Log, Input, Validator;
use Session, Redirect, Hash;

class TimecardController extends Controller
{
    // define validation rules
    public static $rules = array(
            'first_name'    => 'required',
            'last_name'     => 'required',
            'email'         => 'required|email',     // |unique:usersrequired and must be unique in the ducks table
            'password'         => 'required',
            'password_confirm' => 'required|same:password'           // required and has to match the password field
        );


    /**
     * Listing Timecards
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info("TimecardController::index()");

        $timecards = Timecard::all();

        return View('timecards.index')->with('timecards', $timecards);
    }

    /**
     * Show the form for creating a new Timecard Entry
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Log::info("TimecardsController::create()");

        $projects = Project::lists('name', 'id');

        return View('timecards.create')->with('projects', $projects);
    }

    /**
     * Store a newly created Timecard in DB
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       Log::info("TimecardController::store()");

        $in = Input::all();

        $validator = Validator::make($in, self::$rules);

        // validate
        if ($validator->fails()) {
        
            Log::info("Validation : Failed while Storing Timecard");

            // track the error messages from the validator
            Log::debug($validator->messages());

            // notify
            Session::flash('type', 'Warning');
            Session::flash('caption', 'Timecard');
            Session::flash('content', 'Validation Failed');

            // redirect
            return Redirect::to('timecard/create')
                ->withErrors($validator)
                ->withInput($in);
        } else {

            // store
            $timecard = new Timecard();
            $timecard->project_id = $in['project_id'];
            $timecard->user_id    = 1; //TODO : Via Session
            $timecard->save();

            // notify
            Session::flash('type', 'Notify');
            Session::flash('caption', 'Timecard');
            Session::flash('content', 'Created Successfully');
    
            // redirect
            return Redirect::to("timecard");
            // return Redirect::to("timecard/$timecard->id");
        }
    }

    /**
     * Display the specified Timecard
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Log::info("TimecardController::show($id)");

        // retrieve particular Timecard
        $timecard = Timecard::findOrFail($id);
        $projects = Project::lists('name', 'id');

        // show the view and pass the post to it
        return View('timecard.show')
            ->with('projects', $projects);
    }

    /**
     * Show the form for editing the specified Timecard
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Log::info("TimecardController::edit($id)");

        // retrieve particular Timecard
        $timecard = Timecard::findOrFail($id);

        $projects = Project::lists('name', 'id');

        // show the view and pass the post to it
        return View('timecard.edit')
            ->with('projects', $projects);
    }

    /**
     * Update the specified User in DB
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
       Log::info("TimecardController::update($id)");

        $in = Input::all();

        $validator = Validator::make($in, self::$rules);

        // validate
        if ($validator->fails()) {
        
            Log::debug("Validation : Failed while Updating Timecard-Id : $id");

            // track the error messages from the validator
            Log::debug($validator->messages());

            // notify
            Session::flash('type', 'Warning');
            Session::flash('caption', 'Timecard');
            Session::flash('content', 'Validation Failed');
            
            // redirect
            return Redirect::to("timecard/$id/edit")
                ->withErrors($validator)
                ->withInput($in);
        } else {


            // update
            $timecard = Timecard::findOrFail($id);
            $timecard->project_id = $in['project_id'];
            $timecard->user_id    = 1; //TODO : Via Session
            $timecard->save();

            // notify
            Session::flash('type', 'Notify');
            Session::flash('caption', 'Timecard');
            Session::flash('content', 'Updated Successfully');

            // redirect
            return Redirect::to("timecard/$id/edit");
        }

    }

    /**
     * Remove the specified Timecard from DB
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}