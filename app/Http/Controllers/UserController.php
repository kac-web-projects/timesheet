<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

// Additional
use App\Models\User;
use App\Models\Roles;
use Log, Input, Validator;
use Session, Redirect, Hash;

class UserController extends Controller
{
    // define validation rules
    public static $rules = array(
            'first_name'    => 'required',
            'last_name'     => 'required',
            'email'         => 'required|email',     // |unique:usersrequired and must be unique in the ducks table
            'password'         => 'required',
            'password_confirm' => 'required|same:password'           // required and has to match the password field
        );


    /**
     * Listing Users
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info("UserController::index()");

        $users = User::all();

        return View('users.index')->with('users',$users);
    }

    /**
     * Show the form for creating a new User
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Log::info("UserController::create()");

        $roles = Roles::lists('name', 'id');

        return View('users.create')->with('roles', $roles);
    }

    /**
     * Store a newly created User in DB
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       Log::info("UserController::store()");

        $in = Input::all();

        $validator = Validator::make($in, self::$rules);

        // validate
        if ($validator->fails()) {
        
            Log::info("Validation : Failed while Storing User");

            // track the error messages from the validator
            Log::debug($validator->messages());

            // notify
            Session::flash('type', 'Warning');
            Session::flash('caption', 'User');
            Session::flash('content', 'Validation Failed');

            // redirect
            return Redirect::to('user/create')
                ->withErrors($validator)
                ->withInput($in);
        } else {

            $password = $in['password'];

            if (Hash::needsRehash($password)) {
                $password = Hash::make($password);
            }

            // store
            $user = new User();
            $user->first_name = $in['first_name'];
            $user->last_name  = $in['last_name'];
            $user->password   = $password;
            $user->username   = $in['email'];//Note : Initially Same
            $user->email      = $in['email'];
            $user->role_id    = $in['role_id'];
            $user->save();

            // notify
            Session::flash('type', 'Notify');
            Session::flash('caption', 'User');
            Session::flash('content', 'Created Successfully');
    
            // redirect
            return Redirect::to("user");
            // return Redirect::to("user/$user->id");
        }
    }

    /**
     * Display the specified User
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Log::info("UserController::show($id)");

        // retrieve particular User
        $user = User::findOrFail($id);
        $roles = Roles::lists('name', 'id');

        // show the view and pass the post to it
        return View('users.show')
            ->with('user', $user)
            ->with('roles', $roles);
    }

    /**
     * Show the form for editing the specified User
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Log::info("UserController::edit($id)");

        // retrieve particular user
        $user = User::findOrFail($id);

        $roles = Roles::lists('name', 'id');

        // show the view and pass the post to it
        return View('users.edit')
            ->with('user', $user)
            ->with('roles', $roles);
    }

    /**
     * Update the specified User in DB
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
       Log::info("UserController::update($id)");

        $in = Input::all();

        if($in['password'] == '' && $in['password_confirm'] == '') {
            unset(self::$rules['password']);
            unset(self::$rules['password_confirm']);
        }

        $validator = Validator::make($in, self::$rules);

        // validate
        if ($validator->fails()) {
        
            Log::debug("Validation : Failed while Updating User-Id : $id");

            // track the error messages from the validator
            Log::debug($validator->messages());

            // notify
            Session::flash('type', 'Warning');
            Session::flash('caption', 'User');
            Session::flash('content', 'Validation Failed');
            
            // redirect
            return Redirect::to("user/$id/edit")
                ->withErrors($validator)
                ->withInput($in);
        } else {


            // update
            $user = User::findOrFail($id);
            $user->first_name = $in['first_name'];
            $user->last_name  = $in['last_name'];

            if($in['password'] != '' && $in['password_confirm'] !='' ) {

                $password = $in['password'];

                if (Hash::needsRehash($password)) {
                    $password = Hash::make($password);
                }

                $user->password   = $password;
            }

            $user->username   = $in['username'];
            $user->email      = $in['email'];
            $user->role_id    = $in['role_id'];
            $user->save();

            // notify
            Session::flash('type', 'Notify');
            Session::flash('caption', 'User');
            Session::flash('content', 'Updated Successfully');

            // redirect
            return Redirect::to("user/$id/edit");
        }

    }

    /**
     * Remove the specified User from DB
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}