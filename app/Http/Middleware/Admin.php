<?php

namespace App\Http\Middleware;

use Closure;
// Additional
use Log;
use Session, Redirect;
use Auth, User, View;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $date = new DateTime();
        // Log::info("Admin Check @ : ". $date->format('Y-m-d H:i:s'));

        if (Auth::check()) {

            $user = Auth::user();
            if( !$user->isAdmin()) {

                Session::flash('type', 'Danger');
                Session::flash('caption', 'Admin');
                Session::flash('content', 'Access Denied - Need Admin User');
            
                return View::make('errors.401');
            } 

        }
        else {

            Log::info("Guest User, Redirecting to Login");
            return Redirect::guest('login');
        }

        return $next($request);
    }
}
