<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

// Additional
use Log;
use Session, Redirect;

class Authenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $date = new DateTime();
        // Log::info("Authentication Check @ : ". $date->format('Y-m-d H:i:s'));

        if ($this->auth->guest()) {

            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            }
        }
        else {

            Log::info("Guest User, Redirecting to Login");
            
            Session::flash('type', 'Danger');
            Session::flash('caption', 'Access');
            Session::flash('content', 'Need Authenticated User');

            return redirect()->guest('login');
        }

        return $next($request);
    }
}
