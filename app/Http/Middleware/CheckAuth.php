<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Log;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $destination = $request->path();

        Log::info("CheckAuth::handle(), destination : $destination");

        if (Auth::check()) {
            return $next($request);
        } else {
            return redirect("login?destination=$destination");
        }

    }
}
