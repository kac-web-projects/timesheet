<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

//Home
Route::get('/', array('uses' => 'PostsController@newsFeed'));

Route::group(['middleware' => '\App\Http\Middleware\CheckAuth'], function () {

    // Filter Added Already OR not Applicable
    Route::resource('post', 'PostsController');
    Route::resource('role', 'RolesController');
    Route::resource('user', 'UserController');
    Route::resource('project', 'ProjectController');
    Route::resource('timecard', 'TimecardController');

    //Service
    Route::get('comment/operations', 'CommentService@operations');

    //Dashboard
    Route::get('dashboard', array('uses' => 'AdminController@showDashboard'));
    Route::get('debug', array('uses' => 'AdminController@showDebug'));
    Route::get('cmd/{type}', array('uses' => 'AdminController@runCmd'));

});

/*
|--------------------------------------------------------------------------
| Authentication URIs
|--------------------------------------------------------------------------
| Login, Logout, Reset Password
|
 */
Route::get('login', array('uses' => 'LoginController@showLogin'));
Route::post('login', array('uses' => 'LoginController@doLogin'));
Route::get('logout', array('uses' => 'LoginController@doLogOut'));
