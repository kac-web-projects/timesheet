<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'projects';


	/**
	 * Timestamps field such as created_at and updated_at if required.
	 *
	 * @var boolean
	 */
	public $timestamps = true;

   
    /**
     * Relationship with User Table (One to One)
     */
    public function manager()
    {
        return $this->hasOne('App\Models\User','id','user_id');
    }

}