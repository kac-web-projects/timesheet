<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use App\Models\Roles;
use Log;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Timestamps field such as created_at and updated_at if required.
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Relationship with Roles Table (One to One)
     */
    public function role()
    {
        return $this->hasOne('App\Models\Roles','id','role_id');
    }


    /**
     * Check if User has Admin Role
     *
     * @return boolean
     */
    public function isAdmin() {

        // TODO : Use Str::lower($this->role->name)
        if($this->role->name == 'admin') {
           
            Log::info("User is Admin");
            return true;
        }

        return false;
    }

    /**
     * Check if User has PayrollAdmin Role
     *
     * @return boolean
     */
    public function isPayrollAdmin() {

        // TODO : Use Str::lower($this->role->name)
        if($this->role->name == 'payroll-admin') {

            Log::info("User is PayrollAdmin");
            return true;
        }
            
        return false;
    }

    /**
     * Check if User is Owner of Post, Comment
     *
     * @return boolean
     */
    public function isOwner($model, $recordId) {

        Log::info("isOwner check. Model : $model, ID : $recordId");
        
        $flag = false;

        switch ($model) {
            case 'Post':
                $post = Post::find($recordId)->first()->user_id;
                $flag = (Auth::id() == $post) ? 1 : 0;
                break;
            
            case 'Comment':
                $comment = Comment::find($recordId)->first()->user_id;
                $flag = (Auth::id() == $comment) ? 1 : 0;
                break;
        }

        Log::info("isOwner check ? $flag");
        return $flag;
    }
}