<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/**
 * User Model
 */

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {

    return [
        'created_at' => $faker->date($format = 'Y-m-d H:i:s', $max = 'now'),
        'updated_at' => $faker->date($format = 'Y-m-d H:i:s', $max = 'now'),
        'username' => $faker->userName,
        'password' => bcrypt('dummy'),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->freeEmail,
        'remember_token' => str_random(10),
        'role_id' => rand(1, 5),
        'deleted_at' => $faker->date($format = 'Y-m-d H:i:s', $max = 'now'),
    ];
});


/**
 * Role Model
 */

$factory->define(App\Models\Roles::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->text(5),
        'description' => $faker->paragraph($nbSentences = 1),
    ];
});


/**
 * Post Model
 */

$factory->define(App\Models\Post::class, function (Faker\Generator $faker) {

    return [
        'created_at' => $faker->date($format = 'Y-m-d H:i:s', $max = 'now'),
        'updated_at' => $faker->date($format = 'Y-m-d H:i:s', $max = 'now'),
        'post_title' => $faker->sentence(),
        'post_content' => $faker->paragraph($nbSentences = 7),
        'post_status' => 'publish',
        'user_id' => 1,
        'image_path' => $faker->image($dir = '/tmp', $width = 640, $height = 480),
        'comment_status' => 1,
        'comment_count' => 10,
    ];
});


/**
 * Comment Model
 */

$factory->define(App\Models\Comment::class, function (Faker\Generator $faker) {

    return [
        'created_at' => $faker->date($format = 'Y-m-d H:i:s', $max = 'now'),
        'updated_at' => $faker->date($format = 'Y-m-d H:i:s', $max = 'now'),
        'comment_content' => $faker->paragraph($nbSentences = 1),
        'post_id' => 1,
        'user_id' => 1,
    ];
});


/**
 * Project Model
 */

$factory->define(App\Models\Project::class, function (Faker\Generator $faker) {

    return [
        'created_at' => $faker->date($format = 'Y-m-d H:i:s', $max = 'now'),
        'updated_at' => $faker->date($format = 'Y-m-d H:i:s', $max = 'now'),
        'name' => $faker->text(10),
        'description' => $faker->paragraph($nbSentences = 2),
        'status' => 1,
        'user_id' => 1,
        'deleted_at' => $faker->date($format = 'Y-m-d H:i:s', $max = 'now'),
    ];
});


/**
 * Timecard Model
 */

$factory->define(App\Models\Timecard::class, function (Faker\Generator $faker) {

    return [
        'created_at' => $faker->date($format = 'Y-m-d H:i:s', $max = 'now'),
        'updated_at' => $faker->date($format = 'Y-m-d H:i:s', $max = 'now'),
        'project_id' => 1,
        'user_id' => 1,
    ];
});