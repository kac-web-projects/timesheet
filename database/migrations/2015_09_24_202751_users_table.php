<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            
            $table->increments('id');
            $table->timestamps();   //created_at, updated_at
            
            $table->string('username', 60)->unique();
            $table->string('password', 60);
            $table->string('first_name', 60);
            $table->string('last_name', 60)->nullable();
            $table->boolean('active');

            $table->string('email')->unique();
            $table->rememberToken();
            
            $table->bigInteger('role_id');

            $table->softDeletes();  //deleted_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
