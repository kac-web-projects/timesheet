<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            
            $table->increments('id');

            $table->string('post_title', 100);
            $table->longText('post_content');
            $table->string('post_status', 20);  //Publish, UnPublish, Draft

            $table->integer('user_id');
            $table->string('image_path')->unique();

            $table->boolean('comment_status');
            $table->integer('comment_count');

            $table->timestamps();   //created_at, updated_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
