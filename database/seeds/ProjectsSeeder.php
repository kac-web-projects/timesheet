<?php

use Illuminate\Database\Seeder;

class ProjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$projects = [
		    [ 'name' => 'reliance',
		    'description' => 'Reliance',
		    'status' => '1',
		    'user_id' => '1' ],

		    [ 'name' => 'reliance',
		    'description' => 'Reliance',
		    'status' => '1',
		    'user_id' => '1' ]
		];

        DB::table('projects')->insert($projects);
       
    }
}
