<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [ 'name' => 'admin',
            'description' => 'All operations permitted. Role, User, Project Management ' ],

			[ 'name' => 'payroll-admin',
            'description' => 'Operations such as User & Timecard management' ],

            [ 'name' => 'project-manager',
            'description' => 'Operations such as manage project, approve/edit/delete timecard' ],

            [ 'name' => 'employee',
            'description' => 'System User; Timecard Entry' ]

        ];

        DB::table('roles')->insert($roles);
       
    }
}
