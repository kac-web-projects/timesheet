<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$users = [
            [ 'created_at' => new Datetime(),
            'username' => 'media',
            // media@timesheet
            'password' => '$2y$10$6IEmVRD/07OemAcHs/2bK.qJss0hv7oJsxN7f/23521p9O50myqGi',
            'first_name' => 'media',
            'last_name' => 'master',
            'active' => 1,
            'email' => 'media@gmail.com',
            'remember_token' => NULL,
            'role_id' => 1 ],

            [ 'created_at' => new Datetime(),
            'username' => 'norman',
            'password' => '$2y$10$XNDV/.JGIxP3bINzIk038uXU7zZo1PndXFuuYpMQdUPJwqekgSX3G',
            'first_name' => 'norman',
            'last_name' => 'cote',
            'active' => 1,
            'email' => 'norman@gmail.com',
            'remember_token' => NULL,
            'role_id' => 2 ],

            [ 'created_at' => new Datetime(),
			'username' => 'chris',
            'password' => '$2y$10$q/QqvsrhstDzyVsT5t8ePegnodINbdqXClFgaqweoE9mPRXoY61bS',
            'first_name' => 'chris',
            'last_name' => 'roger',
            'active' => 1,
            'email' => 'chris@gmail.com',
            'remember_token' => NULL,
            'role_id' => 3 ],

            [ 'created_at' => new Datetime(),
            'username' => str_random(10),
            'password' => bcrypt('secret'),
            'first_name' => str_random(10),
            'last_name' => str_random(10),
            'active' => 1,
            'email' => str_random(10).'@gmail.com',
            'remember_token' => NULL,
            'role_id' => 1 ],

            [ 'created_at' => new Datetime(),
            'username' => str_random(10),
            'password' => bcrypt('secret'),
            'first_name' => str_random(10),
            'last_name' => str_random(10),
            'active' => 1,
            'email' => str_random(10).'@gmail.com',
            'remember_token' => NULL,
            'role_id' => 1 ],

            [ 'created_at' => new Datetime(),
            'username' => str_random(10),
            'password' => bcrypt('secret'),
            'first_name' => str_random(10),
            'last_name' => str_random(10),
            'active' => 1,
            'email' => str_random(10).'@gmail.com',
            'remember_token' => NULL,
            'role_id' => 1 ],

            [ 'created_at' => new Datetime(),
            'username' => str_random(10),
            'password' => bcrypt('secret'),
            'first_name' => str_random(10),
            'last_name' => str_random(10),
            'active' => 1,
            'email' => str_random(10).'@gmail.com',
            'remember_token' => NULL,
            'role_id' => 1 ],

            [ 'created_at' => new Datetime(),
            'username' => str_random(10),
            'password' => bcrypt('secret'),
            'first_name' => str_random(10),
            'last_name' => str_random(10),
            'active' => 1,
            'email' => str_random(10).'@gmail.com',
            'remember_token' => NULL,
            'role_id' => 1 ],

            [ 'created_at' => new Datetime(),
            'username' => str_random(10),
            'password' => bcrypt('secret'),
            'first_name' => str_random(10),
            'last_name' => str_random(10),
            'active' => 1,
            'email' => str_random(10).'@gmail.com',
            'remember_token' => NULL,
            'role_id' => 1 ],

            [ 'created_at' => new Datetime(),
            'username' => str_random(10),
            'password' => bcrypt('secret'),
            'first_name' => str_random(10),
            'last_name' => str_random(10),
            'active' => 1,
            'email' => str_random(10).'@gmail.com',
            'remember_token' => NULL,
            'role_id' => 1 ]
        
        ];

        DB::table('users')->insert($users);
    }
}