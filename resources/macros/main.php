<?php

//Usage : Blank Spaces for HTML
HTML::macro('spaces', function($limit)
{
	$html = '';

	for($i = 0; $i < $limit; $i++) {
		$html .= '&nbsp;';
	}

	return $html;
});

//Usage : Blank Rows
HTML::macro('blankRows', function($limit)
{
	$html = '';

	for($i = 0; $i < $limit; $i++) {
		$html .= '<br/>';
	}

	return $html;
});

Form::macro('sumthin', function()
{
    return '<input type="sumthin" value="default">';
});