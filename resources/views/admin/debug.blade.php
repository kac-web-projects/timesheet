@extends('_layouts.default')

@section('title', 'Debug GUI')

@section('content')

<div class="grid">

    <div class="row cells12" >
		
		<h2 class="align-left"><a href="{!! URL::previous() !!}" class="mif-backward bg-white fg-black"></a> Debug GUI</h2>

        <div class="row cell12">
        	
        	<h1>Storage</h1>
	            <div class="cell4">
		            <a href="{!! URL::to('cmd/view-clear') !!}" class="button primary"><span class="mif-plus">&nbsp;</span>Rebuild Views</a>
		            <a href="{!! URL::to('cmd/clear-session') !!}" class="button primary"><span class="mif-plus">&nbsp;</span>Clear Session</a>
	        	</div>

        	<h1>Migration</h1>
	
	        	 <div class="cell4">
		            <a href="{!! URL::to('cmd/migrate-refresh') !!}" class="button primary">Refresh</a>
		            <a href="{!! URL::to('cmd/migrate-reset') !!}" class="button primary">Reset</a>
		            <a href="{!! URL::to('cmd/migrate-rollback') !!}" class="button primary">RollBack</a>
		            <a href="{!! URL::to('cmd/migrate-status') !!}" class="button primary">Status</a>
	        	</div>

        	</div>

        	<h1>Database</h1>
	
	        	 <div class="cell4">
		            <a href="{!! URL::to('cmd/rebuild-migrations') !!}" class="button primary">Reset & Run Migration</a>
		            <a href="{!! URL::to('cmd/run-seeds') !!}" class="button primary">Run Seeds</a>
	        	</div>

        	</div>


        	<h1>Application Routes</h1>

			    <div class="cell4">
		            <a href="{!! URL::to('routes') !!}" class="button primary">Routes</a>
	        	</div>

        </div>
	</div>

</div>
@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

	});

</script>
@stop