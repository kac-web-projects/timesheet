@extends('_layouts.default')
@section('title', 'Login')
@section('content')
<div class="grid">
    <div class="row cells12">
        <h2 class="align-left">
            <a class="mif-backward bg-white fg-black" href="{{ URL::previous() }}">
            </a>
            Login
        </h2>
        <div class="row cells12">
            {!! Form::open(array('action' => array('LoginController@doLogin'))) !!}
            <!--  Username -->
            <div class="row cells2">
                <div class="cell">
                    {!! Form::label('username', 'Username') !!}
                    <div class="input-control text full-size" datad-role="input-control">
                        {!! Form::text('username', null, array('placeHolder' => 'Username || Email')) !!}
                        <button class="button helper-button clear" onclick="return false">
                            <span class="mif-cross">
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- !cell6 -->
            <!--  Password -->
            <div class="row cells3">
                <div class="cell">
                    {!! Form::label('password', 'Password') !!}
                    <div class="input-control text full-size">
                        {!! Form::password('password', null, array('class' => '')) !!}
                        <button class="button helper-button clear" onclick="return false">
                            <span class="mif-cross">
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- !cells3 -->
            <!--  Remember Me -->
            <div class="row cells2">
                <label class="input-control checkbox small-check">
                    <input name="remember_me" type="checkbox">
                        <span class="check">
                        </span>
                        <span class="caption">
                            Remember me
                        </span>
                    </input>
                </label>
            </div>
        </div>
        <!-- !cell12 -->
        <div class="row cells4">
            <a class="button warning" href="{{ URL::previous() }}">
                Cancel
            </a>

            {!! Form::submit('Proceed', array('name' => 'proceed','class' => 'button primary')) !!}
        </div>
        <!-- !cell4 -->
    </div>
    @if(isset($_GET['destination']))
{!! Form::hidden('destination',$_GET['destination']) !!}
@else
{!! Form::hidden('destination','/') !!}
@endif



{!! Form::close() !!}
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {

	});
    </script>
    @stop
</div>