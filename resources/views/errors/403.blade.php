@extends('_layouts.errors')
@section('title', '403 - Permission Denied')
@section('content')
<div class="grid">
    <div class="row cells12" style="margin-top: 80px">
        <h2 class="align-left">
            <a class="mif-backward bg-white fg-black" href="{{ URL::previous() }}">
            </a>
            Internal Error : 403 - Permission Denied
        </h2>
        <div class="row cell12">
            You do not have permission for this request
        </div>
        <a href="#" id="controller">
            Show More
        </a>
        <div class="row cell12" id="advance" style="display:none">
            @if(Auth::check())
            {!! $exception !!}
            @else
            Internal Error
            @endif
        </div>
    </div>
</div>
@stop

@section('script')
<script type="text/javascript">
    $(document).ready(function() {

        $('#controller').on('click',function() {
            
            $('#advance').toggle();
        });

    });
</script>
@stop
