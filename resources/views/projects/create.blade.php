@extends('_layouts.default')

@section('title', 'New Project')

@section('content')

<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"></a> Add Project</h2>

<!--  Show Errors if applicable -->
<div class="row cells6">
	@if ($errors->has())

    <div class="block-shadow-danger">
        @foreach ($errors->all() as $error)
            {!! $error !!}<br>        
        @endforeach
    </div>
    
    @endif
</div>

<div class="row cells6">
	{!! Form::open(array('url' => 'project')) !!}

	<!--  Project Name -->
	<div class="row cells2">

		<div class="cell">
			{!! Form::label('name','Name') !!}

			<div class="input-control text full-size">
				{!! Form::text('name', null, array('placeHolder' => 'Project name', 'class' => '')) !!}
				
				<button class="button helper-button clear" onClick="return false">
					<span class="mif-cross"></span>
				</button>

			</div>
		</div>

	</div>


	<!--  Project Description -->
	<div class="row cells1">
	
		<div class="cell">
			{!! Form::label('description','Description') !!}

			<div class="input-control textarea full-size">
				
				{!! Form::textarea('description',null,array('placeHolder' => 'Project Info' ,'class' => 'ckeditor')) !!}
			</div>

		</div>

	</div>	

	<!--  User -->
	<div class="row cells2">

		<div class="cell">
			{!! Form::label('manager_id', 'Manager')!!}
			
			<div class="input-control select full-size">
				
				{!! Form::select('manager_id', $users)!!}
			</div>

		</div>
	</div>

	<!--  Status -->
	<div class="row cells2">

		<div class="cell">
			{!! Form::label('manager_id', 'Manager')!!}
			
			<div class="input-control select full-size">
				
				{!! Form::select('manager_id', $users)!!}
			</div>

		</div>
	</div>

	<!--  Active -->
	<div class="row cells2">
		
		<label class="input-control checkbox full-check">
		    <input type="checkbox" checked>
		    <span class="caption">Active</span>
		    <span class="check"></span>
		</label>
	</div>

</div>
<!-- !cell8 -->

<div class="row cells3">

	<a href="{{ URL::previous() }}" class="button warning">Cancel</a>
	{!! Form::submit('Create', array('class' => 'button primary')) !!}

</div><!-- !cell4 -->

{!! Form::close() !!}

@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

		function notifyOnErrorInput(input){
	        var message = input.data('validateHint');
	        $.Notify({
	            caption: 'Error',
	            content: message,
	            type: 'alert'
	        });
	    }


	});

</script>
@stop





