@extends('_layouts.default')

@section('title', 'ALL Projects')
@section('content')

<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"> </a> All Projects</h2>

<div class="row cell12">

	<table id="projects" class="display" cellspacing="0" width="100%">
    
	    <thead>
	        <tr>
	            <th>#</th>
	            <th>Name</th>
	            <th>Desc</th>
	            <th>Status</th>
	            <th>Manager</th>
	            <th>More</th>
	            <th>More</th>
	        </tr>
	    </thead>

	    <tfoot>
	        <tr>
	            <th>#</th>
	            <th>Name</th>
	            <th>Desc</th>
	            <th>Status</th>
	            <th>Manager</th>
	            <th>More</th>
	            <th>More</th>
	        </tr>
	    </tfoot>

	    <tbody>
			@foreach ($projects as $key => $project)
			<tr id="{{ $project->id }}">
				<td> {{ $key + 1 }} </td>
				<td> {{ $project->name }} </td>
				<td> {{ $project->description }} </td>
				<td> {{ $project->status }} </td>

				<!-- Relationship with Role model -->
				<td> {{ $project->manager->username }} </td>
				<td> {{ 'TODO' }} </td>
				<td> {{ 'TODO' }} </td>
			</tr>
			@endforeach
	    </tbody>
	</table>

</div>

@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

		var table = $('#projects').DataTable({
			stateSave: true,
			lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "All"] ]
		});

		$('#projects tbody').on('click', 'tr', function () {
	        
	        var id = $(this).attr('id');
	        
	        window.open("{{ URL::to('project') }}/" + id);
    	} );

	});

</script>
@stop