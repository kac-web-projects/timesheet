@extends('_layouts.default')

@section('title', 'ALL Timecards')

@section('content')

<h2 class="align-left"><a href="{{ URL::previous() }}" class="mif-backward bg-white fg-black"> </a> All Timecards</h2>

<div class="row cell12">

	<table id="timecards" class="display" cellspacing="0" width="100%">
    
	    <thead>
	        <tr>
	            <th>#</th>
	            <th>Project</th>
	            <th>Hours</th>
	            <th>Start Date</th>
	            <th>End Date</th>
	            <th>Status</th>
	        </tr>
	    </thead>

	    <tfoot>
	        <tr>
	            <th>#</th>
	            <th>Project</th>
	            <th>Hours</th>
	            <th>Start Date</th>
	            <th>End Date</th>
	            <th>Status</th>
	        </tr>
	    </tfoot>

	    <tbody>
			@foreach ($timecards as $key => $timecard)
			<tr id="{{ $timecard->id }}">
				<td> {{ $key + 1 }} </td>
				<td> {{ $timecard->project->name }} </td>
				<td> todo </td>
				<td> todo </td>
				<td> todo </td>
				<td> todo </td>
			</tr>
			@endforeach
	    </tbody>
	</table>

</div>

@stop

@section('script')
<script type="text/javascript">

	$(document).ready(function() {

		var table = $('#users').DataTable({
			stateSave: true,
			lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "All"] ]
		});

		$('#timecards tbody').on('click', 'tr', function () {
	        
	        var id = $(this).attr('id');
	        
	        window.open("{{ URL::to('timecard') }}/" + id);
    	} );

	});

</script>
@stop