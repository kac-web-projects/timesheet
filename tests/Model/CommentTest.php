<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

// Models
use App\Models\Comment;

class CommentTest extends TestCase
{
    private static $comments;
    private static $firstComment;
    private static $insertRecord = 8;


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testComment()
    {
        Log::info("CommentTest : testComment()");
        
        self::truncateComment();
        self::countComment('afterTruncate');
        
        self::addComment();
        self::countComment('afterAdd');
        
        self::updateComment();
        self::countComment('afterUpdate');
        
        self::deleteComment();
        self::countComment('afterDelete');

        self::truncateComment();
        self::countComment('afterTruncate');
    }


    /**
     * Truncate Comment Model
     *
     * @return void
     */
    public function truncateComment()
    {
        Log::info("CommentTest : truncateComment()");
        DB::table('comments')->delete();
    }


    /**
     * Add Comment
     *
     * @return void
     */
    public function addComment()
    {
        Log::info("CommentTest : addComment()");
        self::$comments = factory(App\Models\Comment::class, self::$insertRecord)->create();

        //Note : Track 1st Comment
        self::$firstComment = Comment::all()->first();
    }

    
    /**
     * Update Comment
     *
     * @return void
     */
    public function updateComment()
    {
        Log::info("CommentTest : updateComment()");

        $updateComment = Comment::where('comment_content', self::$firstComment['comment_content'])->first();
        $updateComment->comment_content = $updateComment->comment_content . "-> U";
        $updateComment->update();
        
        Log::info("updateComment() : Comment before update");
        Log::debug(self::$firstComment);

        Log::info("CommentTest : updateComment() : Comment after update");
        Log::debug($updateComment);

        //Expectation : Both should NOT be equal
        if( $updateComment != self::$firstComment) {
            $this->assertTrue(true);
        }
        else {
            $this->assertTrue(false);
        }
    }


    /**
     * Delete Comment
     *
     * @return void
     */
    public function deleteComment()
    {
        Log::info("CommentTest : deleteComment()");
        $delete = Comment::where('id', self::$firstComment['id'])->delete();
    }

    
    /**
     * Count Post
     *
     * @return void
     */
    public function countComment($after)
    {
        Log::info("CommentTest : countPost(). After : $after");

        switch ($after) {
            case 'afterTruncate':
                $status = ( 0 == Comment::all()->count() ) ? true : false;
                $this->assertTrue($status);
                break;
            
            case 'afterAdd':
                $status = ( self::$insertRecord == Comment::all()->count()) ? true : false;
                $this->assertTrue($status);
                break;
            
            case 'afterUpdate':
                $status = ( self::$insertRecord == Comment::all()->count() ) ? true : false;
                $this->assertTrue($status);
                break;
            
            case 'afterDelete':
                $status = ( self::$insertRecord - 1 == Comment::all()->count() ) ? true : false;
                $this->assertTrue($status);
                break;
        }

    }


    /**
     * Search Comment
     *
     * @return Comment || Null
     */
    public function searchComment($commentContent)
    {
        Log::info("CommentTest : searchComment()");

    }

}