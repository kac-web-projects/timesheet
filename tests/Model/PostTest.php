<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

// Models
use App\Models\Post;

class PostTest extends TestCase
{
    private static $posts;
    private static $firstPost;
    private static $insertRecord = 3;


    /**
     * An init test.
     *
     * @return void
     */
    public function testPost()
    {
        Log::info("PostTest : testPost()");
        
        self::truncatePost();
        self::countPost('afterTruncate');
        
        self::addPost();
        self::countPost('afterAdd');
        
        self::updatePost();
        self::countPost('afterUpdate');
        
        self::deletePost();
        self::countPost('afterDelete');

        self::truncatePost();
        self::countPost('afterTruncate');
    }


    /**
     * Truncate Post Model
     *
     * @return void
     */
    public function truncatePost()
    {
        Log::info("PostTest : truncatePost()");
        DB::table('posts')->delete();
    }


    /**
     * Add Post
     *
     * @return void
     */
    public function addPost()
    {
        Log::info("PostTest : addPost()");
        self::$posts = factory(App\Models\Post::class, self::$insertRecord)->create();

        //Note : Track 1st Post
        self::$firstPost = Post::all()->first();
    }

    
    /**
     * Update Post
     *
     * @return void
     */
    public function updatePost()
    {
        Log::info("PostTest : updatePost()");

        $updatePost = Post::where('post_title', self::$firstPost['post_title'])->first();
        $updatePost->post_content = $updatePost->post_content . "-> U";
        $updatePost->update();
        
        Log::info("updatePost() : Post before update");
        Log::debug(self::$firstPost);

        Log::info("PostTest : updatePost() : Post after update");
        Log::debug($updatePost);

        //Expectation : Both should NOT be equal
        if( $updatePost != self::$firstPost) {
            $this->assertTrue(true);
        }
        else {
            $this->assertTrue(false);
        }
    }


    /**
     * Delete Post
     *
     * @return void
     */
    public function deletePost()
    {
        Log::info("PostTest : deletePost()");
        $delete = Post::where('post_title', self::$firstPost['post_title'])->delete();
    }

    
    /**
     * Count Post
     *
     * @return void
     */
    public function countPost($after)
    {
        Log::info("PostTest : countPost(). After : $after");

        switch ($after) {
            case 'afterTruncate':
                $status = ( 0 == Post::all()->count() ) ? true : false;
                $this->assertTrue($status);
                break;
            
            case 'afterAdd':
                $status = ( self::$insertRecord == Post::all()->count()) ? true : false;
                $this->assertTrue($status);
                break;
            
            case 'afterUpdate':
                $status = ( self::$insertRecord == Post::all()->count() ) ? true : false;
                $this->assertTrue($status);
                break;
            
            case 'afterDelete':
                $status = ( self::$insertRecord - 1 == Post::all()->count() ) ? true : false;
                $this->assertTrue($status);
                break;
        }

    }


    /**
     * Search Post
     *
     * @return Post || Null
     */
    public function searchPost($postTitle)
    {
        Log::info("PostTest : searchPost()");

    }

}