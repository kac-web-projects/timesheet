<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

// Models
use App\Models\Project;

class ProjectTest extends TestCase
{
    private static $projects;
    private static $firstProject;
    private static $insertRecord = 8;


    /**
     * An init test.
     *
     * @return void
     */
    public function testProject()
    {
        Log::info("ProjectTest : testProject()");
        
        self::truncateProject();
        self::countProject('afterTruncate');
        
        self::addProject();
        self::countProject('afterAdd');
        
        self::updateProject();
        self::countProject('afterUpdate');
        
        self::deleteProject();
        self::countProject('afterDelete');

        self::truncateProject();
        self::countProject('afterTruncate');
    }


    /**
     * Truncate Project Model
     *
     * @return void
     */
    public function truncateProject()
    {
        Log::info("ProjectTest : truncateProject()");
        DB::table('projects')->delete();
    }


    /**
     * Add Project
     *
     * @return void
     */
    public function addProject()
    {
        Log::info("ProjectTest : addProject()");
        self::$projects = factory(App\Models\Project::class, self::$insertRecord)->create();

        //Note : Track 1st Project
        self::$firstProject = Project::all()->first();
    }

    
    /**
     * Update Project
     *
     * @return void
     */
    public function updateProject()
    {
        Log::info("ProjectTest : updateProject()");

        $updateProject = Project::where('name', self::$firstProject['name'])->first();
        $updateProject->description = $updateProject->description . "-> U";
        $updateProject->update();
        
        Log::info("updateProject() : Project before update");
        Log::debug(self::$firstProject);

        Log::info("ProjectTest : updateProject() : Project after update");
        Log::debug($updateProject);

        //Expectation : Both should NOT be equal
        if( $updateProject != self::$firstProject) {
            $this->assertTrue(true);
        }
        else {
            $this->assertTrue(false);
        }
    }


    /**
     * Delete Project
     *
     * @return void
     */
    public function deleteProject()
    {
        Log::info("ProjectTest : deleteProject()");
        $delete = Project::where('name', self::$firstProject['name'])->delete();
    }

    
    /**
     * Count Project
     *
     * @return void
     */
    public function countProject($after)
    {
        Log::info("ProjectTest : countProject(). After : $after");

        switch ($after) {
            case 'afterTruncate':
                $status = ( 0 == Project::all()->count() ) ? true : false;
                $this->assertTrue($status);
                break;
            
            case 'afterAdd':
                $status = ( self::$insertRecord == Project::all()->count()) ? true : false;
                $this->assertTrue($status);
                break;
            
            case 'afterUpdate':
                $status = ( self::$insertRecord == Project::all()->count() ) ? true : false;
                $this->assertTrue($status);
                break;
            
            case 'afterDelete':
                $status = ( self::$insertRecord - 1 == Project::all()->count() ) ? true : false;
                $this->assertTrue($status);
                break;
        }

    }


    /**
     * Search Project
     *
     * @return Project || Null
     */
    public function searchProject($name)
    {
        Log::info("ProjectTest : searchProject()");

    }

}