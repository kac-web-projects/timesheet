<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

// Models
use App\Models\Roles;

class RolesTest extends TestCase
{
    private static $roles;
    private static $firstRole;
    private static $insertRecord = 3;


    /**
     * An init test.
     *
     * @return void
     */
    public function testRoles()
    {
        Log::info("RolesTest : testRoles()");
        
        self::truncateRoles();
        self::countRoles('afterTruncate');
        
        self::addRoles();
        self::countRoles('afterAdd');
        
        self::updateRoles();
        self::countRoles('afterUpdate');
        
        self::deleteRoles();
        self::countRoles('afterDelete');

        self::truncateRoles();
        self::countRoles('afterTruncate');
    }


    /**
     * Truncate Roles Model
     *
     * @return void
     */
    public function truncateRoles()
    {
        Log::info("RolesTest : truncateRoles()");
        DB::table('roles')->delete();
    }


    /**
     * Add Roles
     *
     * @return void
     */
    public function addRoles()
    {
        Log::info("RolesTest : addRoles()");
        self::$roles = factory(App\Models\Roles::class, self::$insertRecord)->create();

        //Note : Track 1st Roles
        self::$firstRole = Roles::all()->first();
    }

    
    /**
     * Update Roles
     *
     * @return void
     */
    public function updateRoles()
    {
        Log::info("RolesTest : updateRoles()");

        $updateRole = Roles::where('name', self::$firstRole['name'])->first();
        $updateRole->description = $updateRole->description . "-> U";
        $updateRole->update();
        
        Log::info("updateRoles() : Roles before update");
        Log::debug(self::$firstRole);

        Log::info("RolesTest : updateRoles() : Roles after update");
        Log::debug($updateRole);

        //Expectation : Both should NOT be equal
        if( $updateRole != self::$firstRole) {
            $this->assertTrue(true);
        }
        else {
            $this->assertTrue(false);
        }
    }


    /**
     * Delete Roles
     *
     * @return void
     */
    public function deleteRoles()
    {
        Log::info("RolesTest : deleteRoles()");
        $delete = Roles::where('name', self::$firstRole['name'])->delete();
    }

    
    /**
     * Count Roles
     *
     * @return void
     */
    public function countRoles($after)
    {
        Log::info("RolesTest : countRoles(). After : $after");

        switch ($after) {
            case 'afterTruncate':
                $status = ( 0 == Roles::all()->count() ) ? true : false;
                $this->assertTrue($status);
                break;
            
            case 'afterAdd':
                $status = ( self::$insertRecord == Roles::all()->count()) ? true : false;
                $this->assertTrue($status);
                break;
            
            case 'afterUpdate':
                $status = ( self::$insertRecord == Roles::all()->count() ) ? true : false;
                $this->assertTrue($status);
                break;
            
            case 'afterDelete':
                $status = ( self::$insertRecord - 1 == Roles::all()->count() ) ? true : false;
                $this->assertTrue($status);
                break;
        }

    }


    /**
     * Search Roles
     *
     * @return Roles || Null
     */
    public function searchRoles($name)
    {
        Log::info("RolesTest : searchRoles()");

    }

}