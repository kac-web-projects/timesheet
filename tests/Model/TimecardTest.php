<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

// Models
use App\Models\Timecard;

class TimecardTest extends TestCase
{
    private static $timecards;
    private static $firstTimecard;
    private static $insertRecord = 20;


    /**
     * An init test.
     *
     * @return void
     */
    public function testTimecard()
    {
        Log::info("TimecardTest : testTimecard()");
        
        self::truncateTimecard();
        self::countTimecard('afterTruncate');
        
        self::addTimecard();
        self::countTimecard('afterAdd');
        
        self::updateTimecard();
        self::countTimecard('afterUpdate');
        
        self::deleteTimecard();
        self::countTimecard('afterDelete');

        self::truncateTimecard();
        self::countTimecard('afterTruncate');
    }


    /**
     * Truncate Timecard Model
     *
     * @return void
     */
    public function truncateTimecard()
    {
        Log::info("TimecardTest : truncateTimecard()");
        DB::table('timecards')->delete();
    }


    /**
     * Add Timecard
     *
     * @return void
     */
    public function addTimecard()
    {
        Log::info("TimecardTest : addTimecard()");
        self::$timecards = factory(App\Models\Timecard::class, self::$insertRecord)->create();

        //Note : Track 1st Timecard
        self::$firstTimecard = Timecard::all()->first();
    }

    
    /**
     * Update Timecard
     *
     * @return void
     */
    public function updateTimecard()
    {
        Log::info("TimecardTest : updateTimecard()");

        $updateTimecard = Timecard::where('id', self::$firstTimecard['id'])->first();
        // $updateTimecard->description = $updateTimecard->description . "-> U";
        $updateTimecard->update();
        
        Log::info("updateTimecard() : Timecard before update");
        Log::debug(self::$firstTimecard);

        Log::info("TimecardTest : updateTimecard() : Timecard after update");
        Log::debug($updateTimecard);

        //Expectation : Both should NOT be equal
        if( $updateTimecard != self::$firstTimecard) {
            $this->assertTrue(true);
        }
        else {
            $this->assertTrue(false);
        }
    }


    /**
     * Delete Timecard
     *
     * @return void
     */
    public function deleteTimecard()
    {
        Log::info("TimecardTest : deleteTimecard()");
        $delete = Timecard::where('id', self::$firstTimecard['id'])->delete();
    }

    
    /**
     * Count Timecard
     *
     * @return void
     */
    public function countTimecard($after)
    {
        Log::info("TimecardTest : countTimecard(). After : $after");

        switch ($after) {
            case 'afterTruncate':
                $status = ( 0 == Timecard::all()->count() ) ? true : false;
                $this->assertTrue($status);
                break;
            
            case 'afterAdd':
                $status = ( self::$insertRecord == Timecard::all()->count()) ? true : false;
                $this->assertTrue($status);
                break;
            
            case 'afterUpdate':
                $status = ( self::$insertRecord == Timecard::all()->count() ) ? true : false;
                $this->assertTrue($status);
                break;
            
            case 'afterDelete':
                $status = ( self::$insertRecord - 1 == Timecard::all()->count() ) ? true : false;
                $this->assertTrue($status);
                break;
        }

    }


    /**
     * Search Timecard
     *
     * @return Timecard || Null
     */
    public function searchTimecard($name)
    {
        Log::info("TimecardTest : searchTimecard()");

    }

}