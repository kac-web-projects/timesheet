<?php

/**
 * @author Ketan Chawda <ketan.a.chawda@gmail.com>
 */

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

// Models
use App\Models\User;

class UserTest extends TestCase
{
    private static $users;
    private static $firstUser;
    private static $insertRecord = 5;

    /**
     * An init test.
     *
     * @return void
     */
    public function testUser()
    {
        Log::info("UserTest : testUser()");
        
        self::truncateUser();
        self::countUser('afterTruncate');
        
        self::addUser();
        self::countUser('afterAdd');
        
        self::updateUser();
        self::countUser('afterUpdate');
        
        self::deleteUser();
        self::countUser('afterDelete');

        self::truncateUser();
        self::countUser('afterTruncate');
    }


    /**
     * Truncate User Model
     *
     * @return void
     */
    public function truncateUser()
    {
        Log::info("UserTest : truncateUser()");
        DB::table('users')->delete();
    }


    /**
     * Add User
     *
     * @return void
     */
    public function addUser()
    {
        Log::info("UserTest : addUser()");
        self::$users = factory(App\Models\User::class, self::$insertRecord)->create();

        //Note : Track 1st User
        self::$firstUser = User::all()->first();
    }

    
    /**
     * Update User
     *
     * @return void
     */
    public function updateUser()
    {
        Log::info("UserTest : updateUser()");

        $updateUser = User::where('username', self::$firstUser['username'])->first();
        $updateUser->first_name = $updateUser->first_name . "-> U";
        $updateUser->last_name = $updateUser->last_name . "-> U";
        $updateUser->update();
        
        Log::info("updateUser() : User before update");
        Log::debug(self::$firstUser);

        Log::info("UserTest : updateUser() : User after update");
        Log::debug($updateUser);

        //Expectation : Both should NOT be equal
        if( $updateUser != self::$firstUser) {
            $this->assertTrue(true);
        }
        else {
            $this->assertTrue(false);
        }
    }


    /**
     * Delete User
     *
     * @return void
     */
    public function deleteUser()
    {
        Log::info("UserTest : deleteUser()");
        $delete = User::where('username', self::$firstUser['username'])->delete();
    }

    
    /**
     * Count User
     *
     * @return void
     */
    public function countUser($after)
    {
        Log::info("UserTest : countUser(). After : $after");

        switch ($after) {
            case 'afterTruncate':
                $status = ( 0 == User::all()->count() ) ? true : false;
                $this->assertTrue($status);
                break;
            
            case 'afterAdd':
                $status = ( self::$insertRecord == User::all()->count()) ? true : false;
                $this->assertTrue($status);
                break;
            
            case 'afterUpdate':
                $status = ( self::$insertRecord == User::all()->count() ) ? true : false;
                $this->assertTrue($status);
                break;
            
            case 'afterDelete':
                $status = ( self::$insertRecord - 1 == User::all()->count() ) ? true : false;
                $this->assertTrue($status);
                break;
        }

    }

    
    /**
     * Search User
     *
     * @return User || Null
     */
    public function searchUser($userName)
    {
        Log::info("UserTest : searchUser()");

    }

}