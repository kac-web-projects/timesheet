<?php

class ApplicationRoutesUrlsTest extends TestCase
{

    /**
     * Visit Application Routes
     * Ex : /routes
     * @return void
     */
    public function testApplicationRoutesRoute()
    {
        // Stop here and mark this test as incomplete.
        $this->markTestIncomplete(
            'This route has not been implemented yet.'
        );
        $user = factory(App\Models\User::class)->create();

        // Task 1 : Acting User from factory class
        $status = $this->actingAs($user)
            ->visit('/routes')
            ->seePageIs('/routes');

    }

}
