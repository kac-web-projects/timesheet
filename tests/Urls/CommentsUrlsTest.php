<?php

class CommentsUrlsTest extends TestCase
{

    /**
     * Visit Comments Route
     * Ex : /comments
     * @return void
     */
    public function testCommentsRoute()
    {
        // Stop here and mark this test as incomplete.
        $this->markTestIncomplete(
            'This route has not been implemented yet.'
        );

        $user = factory(App\Models\User::class)->create();

        // Task 1 : Acting User from factory class
        $status = $this->actingAs($user)
            ->visit('/comments')
            ->seePageIs('/comments');

    }
}
