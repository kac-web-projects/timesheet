<?php

class DashboardUrlsTest extends TestCase
{

    /**
     * Visit Dashboard Route
     * @return void
     */
    public function testDashboardRoute()
    {
        $user = factory(App\Models\User::class)->create();

        // Task 1 : Acting User from factory class
        $status = $this->actingAs($user)
            ->visit('/dashboard')
            ->seePageIs('/dashboard');
            
    }
}
