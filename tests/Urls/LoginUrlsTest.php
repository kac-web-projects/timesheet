<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;

class LoginUrlTest extends TestCase
{
    /**
     * Visit Login Routes
     *
     * @return void
     */
    public function testLoginRoute()
    {
        $this->withoutMiddleware();
        // Task 1 : Check Login
        $status = $this->visit('/login')
            ->seePageIs('/login');
    }

    /**
     * Attempt Login Routes
     *
     * @return void
     */
    public function testLoginSuccessRoute()
    {
        $this->withoutMiddleware();
        // Task 1 : Attempt Login
        $status = $this->visit('/login')
            ->type('media', 'username')
            ->type('media@timesheet', 'password')
            ->check('proceed')
            ->seePageIs('/');
    }

}
