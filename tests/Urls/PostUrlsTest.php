<?php

class PostUrlsTest extends TestCase
{

    /**
     * Visit Post Create Route
     * Ex : /post/create
     * @return void
     */
    public function testPostCreateRoute()
    {
        $user = factory(App\Models\User::class)->create();

        // Task 1 : Acting User from factory class
        $status = $this->actingAs($user)
            ->visit('/post/create')
            ->seePageIs('/post/create');

    }

    /**
     * Visit Post Route
     * Ex : /post
     * @return void
     */
    public function testPostRoute()
    {
        $user = factory(App\Models\User::class)->create();

        // Task 1 : Acting User from factory class
        $status = $this->actingAs($user)
            ->visit('/post')
            ->seePageIs('/post');

    }
}
