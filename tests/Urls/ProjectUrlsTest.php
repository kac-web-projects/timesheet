<?php

class ProjectUrlsTest extends TestCase
{

    /**
     * Visit Project Create Route
     * Ex : /project/create
     * @return void
     */
    public function testProjectCreateRoute()
    {
        $user = factory(App\Models\User::class)->create();

        // Task 1 : Acting User from factory class
        $status = $this->actingAs($user)
            ->visit('/project/create')
            ->seePageIs('/project/create');

    }

    /**
     * Visit Project Route
     * Ex : /project
     * @return void
     */
    public function testProjectRoute()
    {
        $user = factory(App\Models\User::class)->create();

        // Task 1 : Acting User from factory class
        $status = $this->actingAs($user)
            ->visit('/project')
            ->seePageIs('/project');

    }
}
