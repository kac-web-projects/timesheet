<?php

class RoleUrlsTest extends TestCase
{

    /**
     * Visit Role Create Route
     * Ex : /role/create
     * @return void
     */
    public function testRoleCreateRoute()
    {
        $user = factory(App\Models\User::class)->create();

        // Task 1 : Acting User from factory class
        $status = $this->actingAs($user)
            ->visit('/role/create')
            ->seePageIs('/role/create');

    }

    /**
     * Visit Role Route
     * Ex : /role
     * @return void
     */
    public function testRoleRoute()
    {
        $user = factory(App\Models\User::class)->create();

        // Task 1 : Acting User from factory class
        $status = $this->actingAs($user)
            ->visit('/role')
            ->seePageIs('/role');

    }
}
