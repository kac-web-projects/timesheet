<?php

class TimecardUrlsTest extends TestCase
{

    /**
     * Visit Timecard Create Route
     * Ex : /timecard/create
     * @return void
     */
    public function testTimecardCreateRoute()
    {
        $user = factory(App\Models\User::class)->create();

        // Task 1 : Acting User from factory class
        $status = $this->actingAs($user)
            ->visit('/timecard/create')
            ->seePageIs('/timecard/create');

    }

    /**
     * Visit Timecard Route
     * Ex : /timecard
     * @return void
     */
    public function testTimecardRoute()
    {
        $user = factory(App\Models\User::class)->create();

        // Task 1 : Acting User from factory class
        $status = $this->actingAs($user)
            ->visit('/timecard')
            ->seePageIs('/timecard');

    }
}
