<?php

class UserUrlsTest extends TestCase
{

    /**
     * Visit User Create Route
     * Ex : /user/create
     * @return void
     */
    public function testUserCreateRoute()
    {
        $user = factory(App\Models\User::class)->create();

        // Task 1 : Acting User from factory class
        $status = $this->actingAs($user)
            ->visit('/user/create')
            ->seePageIs('/user/create');

    }

    /**
     * Visit User Route
     * Ex : /user
     * @return void
     */
    public function testUserRoute()
    {
        $user = factory(App\Models\User::class)->create();

        // Task 1 : Acting User from factory class
        $status = $this->actingAs($user)
            ->visit('/user')
            ->seePageIs('/user');

    }

    /**
     * Visit ActiveUser Route
     * Ex : /active-users
     * @return void
     */
    public function testActiveUsersRoute()
    {
        // Stop here and mark this test as incomplete.
        $this->markTestIncomplete(
            'This route has not been implemented yet.'
        );
        $user = factory(App\Models\User::class)->create();

        // Task 1 : Acting User from factory class
        $status = $this->actingAs($user)
            ->visit('/active-users')
            ->seePageIs('/active-users');

    }
}
